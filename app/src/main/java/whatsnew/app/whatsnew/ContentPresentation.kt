package whatsnew.app.whatsnew

import android.content.Context
import sys.whatsnew.Content
import whatsnew.app.R
import whatsnew.app.whatsnew.WhatsNewContentSpecs.BAR_CONTENT_ID
import whatsnew.app.whatsnew.WhatsNewContentSpecs.BAZ_CONTENT_ID
import whatsnew.app.whatsnew.WhatsNewContentSpecs.FOO_CONTENT_ID
import java.util.*

data class ContentPresentation(val contentId: UUID) {

    fun getTitle(context: Context) = when (contentId) {
        FOO_CONTENT_ID -> context.getString(R.string.whatsnew_feature_foo_title)
        BAR_CONTENT_ID -> context.getString(R.string.whatsnew_feature_bar_title)
        BAZ_CONTENT_ID -> context.getString(R.string.whatsnew_feature_baz_title)
        else -> null
    }

    fun getDescription(context: Context) = when (contentId) {
        FOO_CONTENT_ID -> context.getString(R.string.whatsnew_feature_foo_description)
        BAR_CONTENT_ID -> context.getString(R.string.whatsnew_feature_bar_description)
        BAZ_CONTENT_ID -> context.getString(R.string.whatsnew_feature_baz_description)
        else -> null
    }

    companion object {

        fun from(content: Content) = ContentPresentation(content.id)

    }

}