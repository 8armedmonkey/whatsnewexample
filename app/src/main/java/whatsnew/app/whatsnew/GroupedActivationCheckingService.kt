package whatsnew.app.whatsnew

import sys.whatsnew.ActivationCheckingService
import sys.whatsnew.ActivationCondition

class GroupedActivationCheckingService : ActivationCheckingService {

    private val entries = mutableMapOf<Class<out ActivationCondition>, ActivationCheckingService>()

    override fun isFulfilled(activationCondition: ActivationCondition): Boolean {
        return entries[activationCondition.javaClass]?.isFulfilled(activationCondition) ?: false
    }

    fun <T : ActivationCondition> register(
        activationConditionClass: Class<T>,
        activationCheckingService: ActivationCheckingService
    ) {
        entries[activationConditionClass] = activationCheckingService
    }

    @Suppress("unused")
    fun <T : ActivationCondition> unregister(activationConditionClass: Class<T>) {
        entries.remove(activationConditionClass)
    }

}