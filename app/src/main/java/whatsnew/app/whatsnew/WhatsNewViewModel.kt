package whatsnew.app.whatsnew

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import sys.whatsnew.Content
import sys.whatsnew.WhatsNewAppService
import javax.inject.Inject

class WhatsNewViewModel : ViewModel() {

    @Inject
    internal lateinit var whatsNewAppService: WhatsNewAppService

    private val coroutineScope = CoroutineScope(Dispatchers.Main + Job())
    private val activeContents = mutableListOf<Content>()
    private val activeContentsLiveData = MutableLiveData<List<ContentPresentation>>()
    private val whatsNewEventLiveData = MutableLiveData<WhatsNewEvent>()

    init {
        coroutineScope.launch {
            activeContents.addAll(
                withContext(Dispatchers.IO) {
                    whatsNewAppService.retrieveActiveContents()
                }
            )

            activeContentsLiveData.postValue(
                activeContents.map {
                    ContentPresentation.from(it)
                }
            )
        }
    }

    override fun onCleared() {
        coroutineScope.cancel()
        super.onCleared()
    }

    fun onCloseRequested() {
        markActiveContentsAsSeen()
        whatsNewEventLiveData.postValue(WhatsNewEvent.WhatsNewClosed())
    }

    fun getActiveContents(): LiveData<List<ContentPresentation>> = activeContentsLiveData

    fun getWhatsNewEvent(): LiveData<WhatsNewEvent> = whatsNewEventLiveData

    private fun markActiveContentsAsSeen() {
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                whatsNewAppService.markAsSeen(
                    whatsNewAppService.retrieveActiveContents().map { it.id }.toSet()
                )
            }
        }
    }

}