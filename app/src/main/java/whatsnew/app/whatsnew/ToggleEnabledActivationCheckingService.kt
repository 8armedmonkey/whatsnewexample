package whatsnew.app.whatsnew

import sys.whatsnew.ActivationCheckingService
import sys.whatsnew.ActivationCondition
import sys.whatsnew.ToggleEnabledCondition
import whatsnew.app.util.feature.FeatureAppService

class ToggleEnabledActivationCheckingService(
    private val featureAppService: FeatureAppService
) : ActivationCheckingService {

    override fun isFulfilled(activationCondition: ActivationCondition): Boolean =
        if (activationCondition is ToggleEnabledCondition) {
            featureAppService.isFeatureEnabled(activationCondition.toggleId)
        } else {
            throw IllegalArgumentException()
        }

}