package whatsnew.app.whatsnew

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import whatsnew.app.R

class WhatsNewContentAdapter :
    ListAdapter<ContentPresentation, WhatsNewContentAdapter.ViewHolder>(ItemDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_whatsnew_content, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(contentPresentation: ContentPresentation) {
            itemView.findViewById<TextView>(R.id.title_text_view).text =
                contentPresentation.getTitle(itemView.context)

            itemView.findViewById<TextView>(R.id.description_text_view).text =
                contentPresentation.getDescription(itemView.context)
        }

    }

    object ItemDiffCallback : DiffUtil.ItemCallback<ContentPresentation>() {

        override fun areItemsTheSame(
            oldItem: ContentPresentation,
            newItem: ContentPresentation
        ): Boolean = oldItem.contentId == newItem.contentId

        override fun areContentsTheSame(
            oldItem: ContentPresentation,
            newItem: ContentPresentation
        ): Boolean = oldItem == newItem

    }

}