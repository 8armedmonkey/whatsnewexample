package whatsnew.app.whatsnew

import whatsnew.app.util.livedata.Event

sealed class WhatsNewEvent : Event<Unit>(Unit) {

    class WhatsNewClosed : WhatsNewEvent()

}