package whatsnew.app.whatsnew

import sys.whatsnew.ActivationCheckingService
import sys.whatsnew.ActivationCondition
import sys.whatsnew.WithinDateRangeCondition
import whatsnew.app.util.environment.EnvironmentAppService

class WithinDateRangeActivationCheckingService(
    private val environmentAppService: EnvironmentAppService
) : ActivationCheckingService {

    override fun isFulfilled(activationCondition: ActivationCondition): Boolean {
        if (activationCondition is WithinDateRangeCondition) {
            val current = environmentAppService.getCurrentTimeMillis()
            val start = activationCondition.startDate?.time
            val end = activationCondition.endDate?.time

            return when {
                start != null && end != null -> current in start..end
                start != null -> start <= current
                end != null -> end >= current
                else -> false
            }
        } else {
            throw IllegalArgumentException()
        }
    }

}