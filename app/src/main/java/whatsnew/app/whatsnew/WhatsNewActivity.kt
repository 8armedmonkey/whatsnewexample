package whatsnew.app.whatsnew

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.activity_whatsnew.*
import whatsnew.app.R
import whatsnew.app.inject.Injector
import whatsnew.app.main.MainActivity

class WhatsNewActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this).get(WhatsNewViewModel::class.java).also {
            Injector.getInstance().inject(it)
        }
    }

    private val adapter = WhatsNewContentAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_whatsnew)
        setUpViews()
        setUpObservers()
    }

    private fun setUpViews() {
        whatsnew_contents_recycler_view.adapter = adapter
        (whatsnew_contents_recycler_view.itemAnimator as SimpleItemAnimator).apply {
            supportsChangeAnimations = false
        }

        PagerSnapHelper().attachToRecyclerView(whatsnew_contents_recycler_view)

        close_button.setOnClickListener {
            viewModel.onCloseRequested()
        }
    }

    private fun setUpObservers() {
        viewModel.getActiveContents().observe(this, Observer {
            adapter.submitList(it.toMutableList())
        })

        viewModel.getWhatsNewEvent().observe(this, Observer {
            if (it is WhatsNewEvent.WhatsNewClosed && it.getContentIfNotHandled() != null) {
                navigateToMain()
            }
        })
    }

    private fun navigateToMain() {
        startActivity(MainActivity.getStartIntent(this))
        finishAffinity()
    }

    companion object {

        fun getStartIntent(context: Context): Intent = Intent(context, WhatsNewActivity::class.java)

    }

}