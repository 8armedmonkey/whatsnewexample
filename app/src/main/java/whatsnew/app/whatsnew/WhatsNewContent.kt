package whatsnew.app.whatsnew

import sys.whatsnew.ActivationCondition
import sys.whatsnew.Content
import java.util.*

class WhatsNewContent(
    id: UUID,
    hasBeenSeen: Boolean,
    activationConditions: List<ActivationCondition>
) : Content(id, hasBeenSeen, activationConditions)