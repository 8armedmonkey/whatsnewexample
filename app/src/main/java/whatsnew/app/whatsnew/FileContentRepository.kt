package whatsnew.app.whatsnew

import android.content.Context
import sys.whatsnew.Content
import sys.whatsnew.ContentRepository
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.util.*

class FileContentRepository(context: Context) : ContentRepository {

    private val file = File(context.filesDir, FILE_NAME).also {
        if (!it.exists()) {
            it.createNewFile()
        }
    }

    override fun markAsSeen(id: UUID) {
        FileWriter(file, true).use { writer ->
            writer.appendln(id.toString())
            writer.flush()
        }
    }

    override fun retrieveUnseen(): List<Content> {
        return FileReader(file).use { reader ->
            reader.useLines { lines ->
                val seenContentIds = lines.toList()

                WhatsNewContentSpecs.ENTRIES.mapNotNull {
                    val hasBeenSeen = seenContentIds.contains(it.id.toString())

                    if (hasBeenSeen) {
                        null
                    } else {
                        WhatsNewContent(
                            id = it.id,
                            hasBeenSeen = hasBeenSeen,
                            activationConditions = it.activationConditions
                        )
                    }
                }
            }
        }
    }

    companion object {

        private const val FILE_NAME = "seen-contents.txt"

    }

}