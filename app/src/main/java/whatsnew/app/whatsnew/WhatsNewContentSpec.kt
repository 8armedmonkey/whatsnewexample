package whatsnew.app.whatsnew

import sys.whatsnew.ActivationCondition
import java.util.*

data class WhatsNewContentSpec(
    val id: UUID,
    val activationConditions: List<ActivationCondition>
)