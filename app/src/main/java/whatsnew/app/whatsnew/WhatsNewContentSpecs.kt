package whatsnew.app.whatsnew

import sys.whatsnew.ToggleEnabledCondition
import whatsnew.app.util.feature.FeatureSpecs
import java.util.*

object WhatsNewContentSpecs {

    val FOO_CONTENT_ID: UUID = UUID.nameUUIDFromBytes("foo".toByteArray())
    val BAR_CONTENT_ID: UUID = UUID.nameUUIDFromBytes("bar".toByteArray())
    val BAZ_CONTENT_ID: UUID = UUID.nameUUIDFromBytes("baz".toByteArray())

    val ENTRIES = listOf(
        WhatsNewContentSpec(
            id = FOO_CONTENT_ID,
            activationConditions = listOf(
                ToggleEnabledCondition(toggleId = FeatureSpecs.FOO_FEATURE_ID)
            )
        ),
        WhatsNewContentSpec(
            id = BAR_CONTENT_ID,
            activationConditions = listOf(
                ToggleEnabledCondition(toggleId = FeatureSpecs.BAR_FEATURE_ID)
            )
        ),
        WhatsNewContentSpec(
            id = BAZ_CONTENT_ID,
            activationConditions = listOf(
                ToggleEnabledCondition(toggleId = FeatureSpecs.BAZ_FEATURE_ID)
            )
        )
    )

}