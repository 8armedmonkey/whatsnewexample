package whatsnew.app.launcher

enum class LauncherNavigationTarget {
    WhatsNew,
    Main
}