package whatsnew.app.launcher

import whatsnew.app.util.livedata.Event

class LauncherNavigationEvent(
    target: LauncherNavigationTarget
) : Event<LauncherNavigationTarget>(target) {

    companion object {

        fun whatsNew(): LauncherNavigationEvent =
            LauncherNavigationEvent(LauncherNavigationTarget.WhatsNew)

        fun main(): LauncherNavigationEvent =
            LauncherNavigationEvent(LauncherNavigationTarget.Main)

    }

}