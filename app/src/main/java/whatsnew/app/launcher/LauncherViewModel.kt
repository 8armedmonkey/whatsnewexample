package whatsnew.app.launcher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import sys.whatsnew.WhatsNewAppService
import javax.inject.Inject

class LauncherViewModel : ViewModel() {

    @Inject
    internal lateinit var whatsNewAppService: WhatsNewAppService

    private val coroutineScope = CoroutineScope(Dispatchers.Main + Job())
    
    private val navigationLiveData = MutableLiveData<LauncherNavigationEvent>().apply {
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                if (whatsNewAppService.hasActiveContents()) {
                    postValue(LauncherNavigationEvent.whatsNew())
                } else {
                    postValue(LauncherNavigationEvent.main())
                }
            }
        }
    }

    override fun onCleared() {
        coroutineScope.cancel()
        super.onCleared()
    }

    fun getNavigationEvent(): LiveData<LauncherNavigationEvent> = navigationLiveData

}