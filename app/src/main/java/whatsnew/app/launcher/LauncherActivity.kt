package whatsnew.app.launcher

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import whatsnew.app.inject.Injector
import whatsnew.app.main.MainActivity
import whatsnew.app.whatsnew.WhatsNewActivity

class LauncherActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this).get(LauncherViewModel::class.java).also {
            Injector.getInstance().inject(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.getNavigationEvent().observe(this, Observer {
            when (it?.getContentIfNotHandled()) {
                LauncherNavigationTarget.WhatsNew -> navigateToWhatsNew()
                LauncherNavigationTarget.Main -> navigateToMain()
            }
        })
    }

    private fun navigateToWhatsNew() {
        startActivity(WhatsNewActivity.getStartIntent(this))
        finishAffinity()
    }

    private fun navigateToMain() {
        startActivity(MainActivity.getStartIntent(this))
        finishAffinity()
    }

    companion object {

        fun getStartIntent(context: Context) = Intent(context, LauncherActivity::class.java)

    }

}