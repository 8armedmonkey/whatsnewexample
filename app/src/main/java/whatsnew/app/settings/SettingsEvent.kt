package whatsnew.app.settings

import whatsnew.app.util.livedata.Event

sealed class SettingsEvent : Event<Unit>(Unit) {

    class Closed : SettingsEvent()

}