package whatsnew.app.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import whatsnew.app.util.environment.EnvironmentAppService
import whatsnew.app.util.feature.FeatureAppService
import javax.inject.Inject

class SettingsViewModel : ViewModel() {

    @Inject
    internal lateinit var featureAppService: FeatureAppService

    @Inject
    internal lateinit var environmentAppService: EnvironmentAppService

    private val coroutineScope = CoroutineScope(Dispatchers.Main + Job())

    private val settingsLiveData = MutableLiveData<List<SettingsPresentation>>().apply {
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                postValue(featureAppService.getFeatureInfoList().entries.map {
                    SettingsPresentation.from(it)
                })
            }
        }
    }

    private val settingsEventLiveData = MutableLiveData<SettingsEvent>()

    override fun onCleared() {
        coroutineScope.cancel()
        super.onCleared()
    }

    fun onFeatureEnabledChanged(featureId: String, enabled: Boolean) {
        featureAppService.setFeatureEnabled(featureId, enabled)
    }

    fun onResetAppRequested() {
        environmentAppService.clearUserData()
    }

    fun onLaunchAppRequested() {
        settingsEventLiveData.postValue(SettingsEvent.Closed())
    }

    fun getSettings(): LiveData<List<SettingsPresentation>> = settingsLiveData

    fun getSettingsEvent(): LiveData<SettingsEvent> = settingsEventLiveData

}