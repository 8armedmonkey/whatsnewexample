package whatsnew.app.settings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import whatsnew.app.R

class SettingsAdapter(
    private val onFeatureEnabledChangedListener: (featureId: String, enabled: Boolean) -> Unit
) : ListAdapter<SettingsPresentation, SettingsAdapter.ViewHolder>(ItemDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_feature, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(settingsPresentation: SettingsPresentation) {
            itemView.findViewById<CheckBox>(R.id.feature_enabled_checkbox).apply {
                text = settingsPresentation.getDisplayName(itemView.context)
                isChecked = settingsPresentation.enabled

                setOnClickListener {
                    onFeatureEnabledChangedListener.invoke(
                        settingsPresentation.featureId,
                        isChecked
                    )
                }
            }
        }

    }

    object ItemDiffCallback : DiffUtil.ItemCallback<SettingsPresentation>() {

        override fun areItemsTheSame(
            oldItem: SettingsPresentation,
            newItem: SettingsPresentation
        ): Boolean = oldItem.featureId == newItem.featureId

        override fun areContentsTheSame(
            oldItem: SettingsPresentation,
            newItem: SettingsPresentation
        ): Boolean = oldItem == newItem

    }

}