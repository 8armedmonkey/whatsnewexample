package whatsnew.app.settings

import android.content.Context
import whatsnew.app.R
import whatsnew.app.util.feature.FeatureInfo

data class SettingsPresentation(
    val featureId: String,
    val enabled: Boolean
) {

    fun getDisplayName(context: Context) =
        context.getString(R.string.feature_display_name, featureId)

    companion object {

        fun from(featureInfo: FeatureInfo) = SettingsPresentation(
            featureId = featureInfo.featureId,
            enabled = featureInfo.enabled
        )

    }

}