package whatsnew.app.settings

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.activity_settings.*
import whatsnew.app.R
import whatsnew.app.inject.Injector
import whatsnew.app.launcher.LauncherActivity

class SettingsActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider(this).get(SettingsViewModel::class.java).also {
            Injector.getInstance().inject(it)
        }
    }

    private val adapter = SettingsAdapter { featureId, enabled ->
        viewModel.onFeatureEnabledChanged(featureId, enabled)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setUpViews()
        setUpObservers()
    }

    private fun setUpViews() {
        settings_recycler_view.adapter = adapter
        (settings_recycler_view.itemAnimator as SimpleItemAnimator).apply {
            supportsChangeAnimations = false
        }

        reset_app_button.setOnClickListener {
            viewModel.onResetAppRequested()
        }

        launch_app_button.setOnClickListener {
            viewModel.onLaunchAppRequested()
        }
    }

    private fun setUpObservers() {
        viewModel.getSettings().observe(this, Observer {
            adapter.submitList(it.toMutableList())
        })

        viewModel.getSettingsEvent().observe(this, Observer {
            if (it is SettingsEvent.Closed && it.getContentIfNotHandled() != null) {
                launchApp()
            }
        })
    }

    private fun launchApp() {
        startActivity(LauncherActivity.getStartIntent(this).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        })
        finishAffinity()
    }

}