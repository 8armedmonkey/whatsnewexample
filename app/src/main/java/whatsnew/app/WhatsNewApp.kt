package whatsnew.app

import android.app.Application
import whatsnew.app.inject.Injector

class WhatsNewApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.init(this)
    }

}