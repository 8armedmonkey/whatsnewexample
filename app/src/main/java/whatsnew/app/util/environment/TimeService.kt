package whatsnew.app.util.environment

interface TimeService {

    fun getCurrentTimeMillis(): Long

}