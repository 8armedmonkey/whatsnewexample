package whatsnew.app.util.environment

object DefaultTimeService : TimeService {

    override fun getCurrentTimeMillis(): Long = System.currentTimeMillis()

}