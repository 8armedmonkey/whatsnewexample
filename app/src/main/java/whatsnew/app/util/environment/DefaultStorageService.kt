package whatsnew.app.util.environment

import android.app.ActivityManager
import android.content.Context

class DefaultStorageService(
    private val context: Context
) : StorageService {

    override fun clearUserData() {
        (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
            .clearApplicationUserData()
    }

}