package whatsnew.app.util.environment

class EnvironmentAppService(
    private val timeService: TimeService,
    private val storageService: StorageService
) {

    fun getCurrentTimeMillis(): Long = timeService.getCurrentTimeMillis()

    fun clearUserData() {
        storageService.clearUserData()
    }

}