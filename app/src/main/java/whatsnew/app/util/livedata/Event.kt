package whatsnew.app.util.livedata

import java.util.concurrent.atomic.AtomicBoolean

// Adapted from Android Developers Blog: https://link.medium.com/f2npiYFN06
abstract class Event<out T>(private val content: T) {

    private val hasBeenHandled = AtomicBoolean(false)

    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled.getAndSet(true)) {
            null
        } else {
            content
        }
    }

    fun peekContent(): T = content

    fun hasBeenHandled(): Boolean = hasBeenHandled.get()

}