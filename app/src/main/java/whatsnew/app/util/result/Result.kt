package whatsnew.app.util.result

sealed class Result<T> {

    data class Success<T>(val value: T) : Result<T>()

    data class Failure<T>(val error: Throwable? = null) : Result<T>()

    companion object {

        fun <T> tryCatch(operation: () -> Result<T>): Result<T> = try {
            operation()
        } catch (t: Throwable) {
            Failure(t)
        }

    }

}