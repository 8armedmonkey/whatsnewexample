package whatsnew.app.util.feature

interface FeatureRepository {

    fun getFeatureInfoList(): FeatureInfoList

    fun isFeatureEnabled(featureId: String): Boolean

    fun setFeatureEnabled(featureId: String, enabled: Boolean)

}