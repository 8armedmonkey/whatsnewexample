package whatsnew.app.util.feature

class FeatureAppService(
    private val featureRepository: FeatureRepository
) {

    fun getFeatureInfoList(): FeatureInfoList {
        return featureRepository.getFeatureInfoList()
    }

    fun isFeatureEnabled(featureId: String): Boolean {
        return featureRepository.isFeatureEnabled(featureId)
    }

    fun setFeatureEnabled(featureId: String, enabled: Boolean) {
        featureRepository.setFeatureEnabled(featureId, enabled)
    }

}