package whatsnew.app.util.feature

import android.content.Context
import com.google.gson.GsonBuilder
import java.io.File
import java.io.FileReader
import java.io.FileWriter

class JsonFileFeatureRepository(context: Context) : FeatureRepository {

    private val file = File(context.filesDir, FILE_NAME).also {
        if (!it.exists()) {
            it.createNewFile()
        }
    }
    private val gson = GsonBuilder().serializeNulls().create()

    override fun getFeatureInfoList(): FeatureInfoList = synchronized(this) {
        return FileReader(file).use { reader ->
            val currentFeatureInfoList = try {
                gson.fromJson(reader, FeatureInfoList::class.java)
            } catch (e: Exception) {
                null
            }

            FeatureInfoList(
                entries = FeatureSpecs.FEATURE_IDS.map { featureId ->
                    FeatureInfo(
                        featureId = featureId,
                        enabled = currentFeatureInfoList?.entries?.find {
                            it.featureId == featureId
                        }?.enabled ?: false
                    )
                }
            )
        }
    }

    override fun isFeatureEnabled(featureId: String): Boolean = synchronized(this) {
        getFeatureInfoList().entries.find { it.featureId == featureId }?.enabled ?: false
    }

    override fun setFeatureEnabled(featureId: String, enabled: Boolean) {
        synchronized(this) {
            val newEntries = getFeatureInfoList().entries.map {
                if (it.featureId == featureId) {
                    FeatureInfo(featureId = featureId, enabled = enabled)
                } else {
                    it
                }
            }

            FileWriter(file).use { writer ->
                gson.toJson(FeatureInfoList(entries = newEntries), writer)
                writer.flush()
            }
        }
    }

    companion object {

        private const val FILE_NAME = "features.json"

    }

}