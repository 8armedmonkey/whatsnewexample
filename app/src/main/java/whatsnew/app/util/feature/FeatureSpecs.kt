package whatsnew.app.util.feature

object FeatureSpecs {

    const val FOO_FEATURE_ID = "foo"
    const val BAR_FEATURE_ID = "bar"
    const val BAZ_FEATURE_ID = "baz"

    val FEATURE_IDS = setOf(
        FOO_FEATURE_ID,
        BAR_FEATURE_ID,
        BAZ_FEATURE_ID
    )

}