package whatsnew.app.util.feature

data class FeatureInfo(
    val featureId: String,
    val enabled: Boolean
)