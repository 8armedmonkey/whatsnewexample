package whatsnew.app.util.feature

data class FeatureInfoList(
    val entries: List<FeatureInfo>
)