package whatsnew.app.inject

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(context: Context) {

    private val applicationContext = context.applicationContext

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return applicationContext
    }

}