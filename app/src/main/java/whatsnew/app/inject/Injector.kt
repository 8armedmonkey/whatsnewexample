package whatsnew.app.inject

import android.content.Context
import dagger.Component
import whatsnew.app.launcher.LauncherViewModel
import whatsnew.app.settings.SettingsViewModel
import whatsnew.app.whatsnew.WhatsNewViewModel
import javax.inject.Singleton

@Component(
    modules = [
        ApplicationModule::class,
        EnvironmentModule::class,
        FeatureModule::class,
        WhatsNewModule::class
    ]
)
@Singleton
interface Injector {

    fun inject(viewModel: LauncherViewModel)

    fun inject(viewModel: WhatsNewViewModel)

    fun inject(viewModel: SettingsViewModel)

    companion object {

        private lateinit var instance: Injector

        fun init(context: Context) {
            synchronized(this) {
                if (::instance.isInitialized) {
                    throw IllegalStateException()
                } else {
                    instance = DaggerInjector.builder()
                        .applicationModule(ApplicationModule(context))
                        .build()
                }
            }
        }

        fun getInstance(): Injector = synchronized(this) {
            if (::instance.isInitialized) {
                instance
            } else {
                throw IllegalStateException()
            }
        }

    }

}