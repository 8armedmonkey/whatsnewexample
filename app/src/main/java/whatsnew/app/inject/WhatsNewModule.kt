package whatsnew.app.inject

import android.content.Context
import dagger.Module
import dagger.Provides
import sys.whatsnew.*
import whatsnew.app.util.environment.EnvironmentAppService
import whatsnew.app.util.feature.FeatureAppService
import whatsnew.app.whatsnew.FileContentRepository
import whatsnew.app.whatsnew.GroupedActivationCheckingService
import whatsnew.app.whatsnew.ToggleEnabledActivationCheckingService
import whatsnew.app.whatsnew.WithinDateRangeActivationCheckingService
import javax.inject.Singleton

@Module
class WhatsNewModule {

    @Provides
    @Singleton
    fun provideContentRepository(context: Context): ContentRepository {
        return FileContentRepository(context)
    }

    @Provides
    @Singleton
    fun provideActivationCheckingService(
        featureAppService: FeatureAppService,
        environmentAppService: EnvironmentAppService
    ): ActivationCheckingService {
        return GroupedActivationCheckingService().apply {
            register(
                ToggleEnabledCondition::class.java,
                ToggleEnabledActivationCheckingService(featureAppService)
            )
            register(
                WithinDateRangeCondition::class.java,
                WithinDateRangeActivationCheckingService(environmentAppService)
            )
        }
    }

    @Provides
    @Singleton
    fun provideWhatsNewAppService(
        contentRepository: ContentRepository,
        activationCheckingService: ActivationCheckingService
    ): WhatsNewAppService {
        return WhatsNewAppService(contentRepository, activationCheckingService)
    }

}