package whatsnew.app.inject

import android.content.Context
import dagger.Module
import dagger.Provides
import whatsnew.app.util.environment.*
import javax.inject.Singleton

@Module
class EnvironmentModule {

    @Provides
    @Singleton
    fun provideTimeService(): TimeService {
        return DefaultTimeService
    }

    @Provides
    @Singleton
    fun provideStorageService(context: Context): StorageService {
        return DefaultStorageService(context)
    }

    @Provides
    @Singleton
    fun provideEnvironmentAppService(
        timeService: TimeService,
        storageService: StorageService
    ): EnvironmentAppService {
        return EnvironmentAppService(timeService, storageService)
    }

}