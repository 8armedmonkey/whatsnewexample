package whatsnew.app.inject

import android.content.Context
import dagger.Module
import dagger.Provides
import whatsnew.app.util.feature.FeatureAppService
import whatsnew.app.util.feature.FeatureRepository
import whatsnew.app.util.feature.JsonFileFeatureRepository
import javax.inject.Singleton

@Module
class FeatureModule {

    @Provides
    @Singleton
    fun provideFeatureRepository(context: Context): FeatureRepository {
        return JsonFileFeatureRepository(context)
    }

    @Provides
    @Singleton
    fun provideFeatureAppService(featureRepository: FeatureRepository): FeatureAppService {
        return FeatureAppService(featureRepository)
    }

}